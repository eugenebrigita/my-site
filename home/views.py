from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def home(self):
    return HttpResponse("test home page")

def activity(self):
    return HttpResponse("test activity page")

def experience(self):
    return HttpResponse("test experience page")

def achievement(self):
    return HttpResponse("test achievement page")