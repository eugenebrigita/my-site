from django.test import TestCase, Client

# Create your tests here.
class LandingPageTest(TestCase):
    def test_home_page_is_exist(self):
        response = Client().get("/")
        self.assertEquals(response.status_code, 200)

    def test_activity_page_is_exist(self):
        response = Client().get("/activity")
        self.assertEquals(response.status_code, 200)

    def test_experience_page_is_exist(self):
        response = Client().get("/experience")
        self.assertEquals(response.status_code, 200)

    def test_achievement_page_is_exist(self):
        response = Client().get("/achievement")
        self.assertEquals(response.status_code, 200)