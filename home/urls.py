from django.urls import path
from django.conf import settings
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.home(), name="home"),
    path('activity', views.activity(), name="activity"),
    path('experience', views.experience(), name="experience"),
    path('achievement', views.achievement(), name="achievement")
]